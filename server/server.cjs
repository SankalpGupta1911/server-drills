const http = require("http");
const uuid = require("uuid");
const path = require("path");
const fs = require("fs").promises;

function server(req, res) {
  const url = req.originalUrl;

  if (url === "/" && req.method === "GET") {
    res.status(200);
    const file = path.join(__dirname, "../public/homepage.html");
    fs.readFile(file)
      .then((data) => {
        res.setHeader("Content-Type", "text/html");
        res.end(data);
      })
      .catch((err) => {
        console.error(err);
        res.status(500);
        res.setHeader("Content-type", "text/plain");
        res.end("Unexpected Error");
      });
  } else if (url === "/html" && req.method === "GET") {
    res.status(200);
    const file = path.join(__dirname, "../public/index.html");
    fs.readFile(file)
      .then((data) => {
        res.setHeader("Content-Type", "text/html");
        res.end(data);
      })
      .catch((err) => {
        console.error(err);
        res.status(500);
        res.setHeader("Content-type", "text/plain");
        res.end("Unexpected Error");
      });
  } else if (url === "/json" && req.method === "GET") {
    res.status(200);
    const file = path.join(__dirname, "../public/index.json");
    fs.readFile(file)
      .then((data) => {
        res.setHeader("Content-Type", "application/json");
        res.end(data);
      })
      .catch((err) => {
        console.error(err);
        res.status(500);
        res.setHeader("Content-type", "text/plain");
        res.end("Unexpected Error");
      });
  } else if (url === "/uuid" && req.method === "GET") {
    res.status(200);
    try {
      res.setHeader("Content-Type", "text/plain");
      res.end(`This is a random generated UUID: ${uuid.v4()}`);
    } catch (err) {
      console.error(err);
      res.status(500);
      res.set("Content-Type", "text/plain");
      res.end("Unexpected Error");
    }
  }  else if (url.startsWith("/status") && req.method === "GET") {
    res.status(200);
    res.setHeader("Content-Type", "text/plain");
    try {
      const sCode = url.split("/")[2];
      if (http.STATUS_CODES[sCode] !== undefined) {
        res.end(
          `The status code: ${sCode} stands for: ${http.STATUS_CODES[sCode]}`
        );
      } else {
        res.end(
          `${sCode} is not a valid status code, please input a valid status code!`
        );
      }
    } catch (err) {
      res.status(500);
      res.end("Unexpected Error");
    }
  } else if (url.startsWith("/delay") && req.method === "GET") {
    res.status(200);
    res.setHeader("Content-Type", "text/plain");
    try {
      const timer = url.split("/")[2];
      if (!isNaN(timer) && timer > 0) {
        setTimeout(() => {
          res.end(`This content was displayed after ${timer} seconds!`);
        }, timer * 1000);
      } else {
        res.end(`Please enter a valid positive integer as a timer!`);
      }
    } catch (err) {
      res.status(500);
      res.end("Unexpected Error");
    }
  } else {
    res.status(500);
    res.setHeader("Content-type", "text/plain");
    res.end("Unexpected Error");
  }
}

module.exports = server;
